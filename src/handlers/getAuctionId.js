import AWS from 'aws-sdk';
import commonMiddleware from '../lib/commonMiddleware';
import createError from 'http-errors';

const dynamodb = new AWS.DynamoDB.DocumentClient();

export async function getAuctionById(id) {
  let auctionId;

  try {
    const result = await dynamodb.get({
      TableName: process.env.AUCTIONS_TABLE_NAME,
      Key: { id: id }
    }).promise();

    auctionId = result.Item;
  } catch (error) {
    console.error(error);
    throw new createError.InternalServerError(error);
  }

  if (!auctionId) {
    throw new createError.NotFound(`Auction with ID "${id}" not found!`);
  }

  return auctionId;
}

async function getAuctionId(event, context) {
  const { id } = event.pathParameters;
  const auction = await getAuctionById(id);


  return {
    statusCode: 200,
    body: JSON.stringify(auction),
  };
}

export const handler = commonMiddleware(getAuctionId);


