import AWS from 'aws-sdk';
import commonMiddleware from '../lib/commonMiddleware';
import createError from 'http-errors';

const dynamodb = new AWS.DynamoDB.DocumentClient();

async function getAuction(event, context) {
  let auction;

try {
    const result = await dynamodb.scan({ 
        TableName: process.env.AUCTIONS_TABLE_NAME 
    }).promise();

    auction = result.Items;

} catch (error) {
    console.error(error);
    throw new createError.InternalServerError(error);
}


  return {
    statusCode: 200,
    body: JSON.stringify(auction),
  };
}

export const handler = commonMiddleware(getAuction);


